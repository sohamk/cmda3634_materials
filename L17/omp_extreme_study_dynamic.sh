#!/bin/bash
#SBATCH -A cmda3634_rjh
#SBATCH -p normal_q
#SBATCH -t 00:10:00
#SBATCH --cpus-per-task=128
#SBATCH -o omp_extreme_study_dynamic.out
##SBATCH --exclusive

# Go to the directory where the job was submitted
cd $SLURM_SUBMIT_DIR

# Load the modules
module load matplotlib

# Build the executable
gcc -DDYNAMIC -DSTUDY -o omp_extreme omp_extreme.c vec.c -lm -fopenmp

# OpenMP settings
export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
export OMP_PROC_BIND=TRUE

# run omp_extreme
cat mnist_test.dat | ./omp_extreme 5000 1
cat mnist_test.dat | ./omp_extreme 5000 2
cat mnist_test.dat | ./omp_extreme 5000 4
cat mnist_test.dat | ./omp_extreme 5000 8
cat mnist_test.dat | ./omp_extreme 5000 16
cat mnist_test.dat | ./omp_extreme 5000 32
cat mnist_test.dat | ./omp_extreme 5000 64
cat mnist_test.dat | ./omp_extreme 5000 128
