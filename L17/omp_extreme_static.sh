#!/bin/bash
#SBATCH -A cmda3634_rjh
#SBATCH -p normal_q
#SBATCH -t 00:10:00
#SBATCH --cpus-per-task=4
#SBATCH -o omp_extreme_static.out

# Go to the directory where the job was submitted
cd $SLURM_SUBMIT_DIR

# Load the modules
module load matplotlib

# Build the executable
gcc -o omp_extreme omp_extreme.c vec.c -lm -fopenmp

# OpenMP settings
export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
export OMP_PROC_BIND=spread
export OMP_PLACES=cores

# run omp_extreme
cat mnist_test.dat | ./omp_extreme 2000 1
cat mnist_test.dat | ./omp_extreme 2000 2
cat mnist_test.dat | ./omp_extreme 2000 4
