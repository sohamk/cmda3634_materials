#!/bin/bash
#SBATCH -A cmda3634_rjh
#SBATCH -p normal_q
#SBATCH -t 00:10:00
#SBATCH --nodes=2
#SBATCH --ntasks-per-node=4
#SBATCH -o mpi_world.out

# Go to the directory where the job was submitted
cd $SLURM_SUBMIT_DIR

# Load the modules we need for MPI
module load matplotlib

# Build the executable
mpicc -o mpi_world mpi_world.c

# run mpi_world on $1 cores
# use mpiexec -n $SLURM_NTASKS to run on all allocated resources
mpiexec -n $1 --map-by ppr:$SLURM_NTASKS_PER_NODE:node ./mpi_world
