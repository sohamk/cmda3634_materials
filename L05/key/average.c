#include <stdio.h>

int main () {
    double number;
    double sum = 0;
    int k = 0;
    while (scanf ("%lf",&number) == 1) {
        k += 1;
        sum += number;
    }
    printf ("The average of your %d numbers is %lf\n",k,sum/k);
}
