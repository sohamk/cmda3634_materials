import time
import sys
import numpy as np

# compute the cost of using centers c1, c2, c3, c4
def centers_cost_sq(pts,c1,c2,c3,c4):
    cost_sq = 0
    n = len(pts)
    for i in range(n):
        dist_sq_1 = np.inner(pts[i]-pts[c1],pts[i]-pts[c1])
        dist_sq_2 = np.inner(pts[i]-pts[c2],pts[i]-pts[c2])
        dist_sq_3 = np.inner(pts[i]-pts[c3],pts[i]-pts[c3])
        dist_sq_4 = np.inner(pts[i]-pts[c4],pts[i]-pts[c4])
        min_dist_sq = min(dist_sq_1,dist_sq_2,dist_sq_3,dist_sq_4)
        cost_sq = max(cost_sq,min_dist_sq)
    return cost_sq

# find the optimal cost by checking each pair of centers
def solve_4center(pts,opt_ctrs,max_check):
    min_cost_sq = float("inf")
    n = len(pts)
    tuples_checked = 0
    for i in range(0,n-3):
        for j in range(i+1,n-2):
            for k in range(j+1,n-1):
                for l in range(k+1,n):
                    cost_sq = centers_cost_sq(pts,i,j,k,l)
                    tuples_checked += 1
                    if (cost_sq < min_cost_sq):
                        min_cost_sq = cost_sq
                        opt_ctrs[0] = i
                        opt_ctrs[1] = j
                        opt_ctrs[2] = k
                        opt_ctrs[3] = l
                    if (tuples_checked == max_check):
                        return (np.sqrt(min_cost_sq),tuples_checked)
    return (np.sqrt(min_cost_sq),tuples_checked)

pts = np.loadtxt(sys.argv[1])
opt_ctrs = np.array([0,0,0,0])
start = time.process_time()
max_check = -1
if (len(sys.argv) > 2):
    max_check = int(sys.argv[2])
(min_cost,tuples_checked) = solve_4center(pts,opt_ctrs,max_check)
elapsed = time.process_time()-start
print ('number of points =',len(pts))
print ('4-tuples checked =',tuples_checked)
print ('elapsed time =',np.round(elapsed,5),'seconds')
print ('4-tuples checked per second =',int(tuples_checked/elapsed))
print ('minimum cost =',np.round(min_cost,5))
print ('optimal centers =',opt_ctrs)
