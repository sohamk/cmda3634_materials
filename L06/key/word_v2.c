#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_NUM_WORDS 100000
#define MAX_WORD_SIZE 40

char words[MAX_NUM_WORDS][MAX_WORD_SIZE+1];

int main (int argc, char** argv) {

    /* read the words from the file */
    int num_words = 0;
    char word[BUFSIZ];
    while (scanf("%s",word) == 1) {
        int n = strlen(word);
        if (n > MAX_WORD_SIZE) {
            printf ("error : word length in file > MAX_WORD_SIZE!\n");
            exit(1);
        }
        /* eliminate case sensitivity */
        for (int i=0;i<n;i++) {
            if ((word[i] >= 'A') && (word[i] <='Z')) {
                word[i] += 'a'-'A';
            }   
        }
        /* get rid of punctuation at end of words */
        if ((word[n-1] == ',') || (word[n-1] == ';') || (word[n-1] == '?') ||
                (word[n-1] == '.') || (word[n-1] == 39)) {
            word[n-1] = 0;
        }
        if (n > 1) {
            if ((word[n-1] == '-') && (word[n-2] == '-')) {
                word[n-2] = 0;
            }
        }
        strcpy(words[num_words],word);
        num_words += 1;
    }

    /* make sure there are enough command line arguments */
    if (argc < 2) {
        printf ("Error : Command usage : %s %s\n",argv[0],"word");
        exit(1);
    }

    /* make sure word in the command line argument is not too long */
    if (strlen(argv[1]) > MAX_WORD_SIZE) {
        printf ("error : command line argument word length > MAX_WORD_SIZE!\n");
        exit(1);
    }
    int i;
    int num = 0;
    for (i=0;i<num_words;i++) {
        if (strcmp(words[i],argv[1]) == 0) {
            num += 1;
        }
    }
    printf ("num occurances of %s is %d\n",argv[1],num);
}
