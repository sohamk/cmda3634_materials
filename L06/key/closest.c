#include <stdio.h>
#include <stdlib.h>
#include <float.h>

int main (int argc, char** argv) {
    if (argc < 2) {
        printf ("Error : Command usage : %s %s\n",argv[0],"center");
        exit(1);
    }
    double center = atof(argv[1]);
    double closest;
    double min_dist_sq = DBL_MAX;
    double number;
    int count = 0;
    while (scanf("%lf",&number) == 1) {
        count += 1;
        double dist_sq = (number-center)*(number-center);
        if (dist_sq < min_dist_sq) {
            closest = number;
            min_dist_sq = dist_sq;
        }
    }
    if (count > 0) {
        printf ("The number closest to %lf is %lf\n",center,closest);
    }
}
