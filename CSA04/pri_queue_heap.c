#include <stdio.h>
#include <stdlib.h>
#include "pri_queue.h"

void pri_queue_init(pri_queue* pq, int max_size) {
    pq->elements = (pri_queue_element*)malloc(max_size*sizeof(pri_queue_element));
    if (pq->elements == 0) {
        printf ("error : malloc failed in pri_queue_init\n");
        exit(1);
    }
    pq->max_size = max_size;
    pq->size = 0;
}

int pri_queue_size(pri_queue* pq) {
    return pq->size;
}

pri_queue_element pri_queue_peek_top(pri_queue* pq) {
    if (pq->size == 0) {
        printf ("error : underflow in pri_queue_peek_top\n");
        exit(1);
    }
    return pq->elements[0];
}

void pri_queue_insert(pri_queue* pq, pri_queue_element element) {
    /* add your code here */
}

void pri_queue_delete_top(pri_queue* pq) {

    /* check for an empty priority queue */
    if (pq->size == 0) {
        printf ("error : underflow in pri_queue_delete_top\n");
        exit(1);
    }

    /* move the last element to the root and decrease size by 1 */
    pq->elements[0] = pq->elements[pq->size-1];
    pq->size -= 1;

    /* start at the root */
    int current = 0;

    /* descend the tree swapping as necessary to maintain maxheap property */
    while (current*2+1<pq->size) {
        /* first consider the left child */
        int child = current*2+1;
        /* handle the case where the right child has higher priority */
        if ((child+1<pq->size) &&
                (pq->elements[child+1].priority > pq->elements[child].priority)) {
            child = child + 1;
        }
        /* check to see if we should swap current and child */
        if (pq->elements[current].priority < pq->elements[child].priority) {
            pri_queue_element temp = pq->elements[current];
            pq->elements[current] = pq->elements[child];
            pq->elements[child] = temp;
        } else {
            /* done if largest child value is less than or equal to ours */
            break;
        }
        /* descend the tree */
        current = child;
    }
}

void pri_queue_free(pri_queue* pq) {
    if (pq->elements == 0) {
        printf ("error : null elements pointer in pri_queue_free\n");
        exit(1);
    }
    free (pq->elements);
    pq->elements = 0;
}
