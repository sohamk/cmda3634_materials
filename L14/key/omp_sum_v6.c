#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

typedef unsigned long long uint64_t;

int main(int argc, char **argv) {

    /* get N and thread_count from command line */
    if (argc < 3) {
        printf ("Command usage : %s %s %s\n",argv[0],"N","thread_count");
        return 1;
    }
    uint64_t N = atol(argv[1]);
    int thread_count = atoi(argv[2]);
    omp_set_num_threads(thread_count);

    /* start the timer */
    double start_time, end_time;
    start_time = omp_get_wtime();

    /* calculate the sum */
    uint64_t sum = 0;

    // The code is currently computing a value of sum = thread_count*N*(N+1)/2
    // Since each thread is computing the entire sum we will not get better performance.
    // Modify the loop on line 31 to distribute the work (approximately) evenly
    // across the threads.  

#pragma omp parallel default(none) shared(N,sum,thread_count)
    {
        int thread_num = omp_get_thread_num();
        for (uint64_t i = 1+thread_num; i <= N;i+=thread_count) {
#pragma omp critical
            {
                sum += i;
            }
        }
    }

    /* stop the timer */
    end_time = omp_get_wtime();

    printf ("thread_count = %d, ",thread_count);
    printf ("elapsed time = %g\n",end_time-start_time);
    printf ("sum = %llu\n",sum);
    printf ("N*(N+1)/2 = %llu\n",(N/2)*(N+1));
}

