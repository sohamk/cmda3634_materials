#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

typedef unsigned long long uint64_t;

int main(int argc, char **argv) {

    /* get N and thread_count from command line */
    if (argc < 3) {
        printf ("Command usage : %s %s %s\n",argv[0],"N","thread_count");
        return 1;
    }
    uint64_t N = atol(argv[1]);
    int thread_count = atoi(argv[2]);
    omp_set_num_threads(thread_count);

    /* start the timer */
    double start_time, end_time;
    start_time = omp_get_wtime();

    /* calculate the sum */
    uint64_t sum = 0;

    // Inside a parallel region there are two types of variables: shared and private.
    // There is a single copy of each shared variable that is used by all threads.  
    // Each thread uses its own local copy of each private variable.  
    // Variables defined inside the parallel pragma are private.
    // Variables not defined inside the parallel pragma are shared.  
    // For each of the following variables, decide if it is shared or private:
    // i : private
    // N : shared
    // sum : shared

#pragma omp parallel
    {
        for (uint64_t i = 1; i <= N;i++) {
            sum += i;
        }
    }

    /* stop the timer */
    end_time = omp_get_wtime();

    printf ("thread_count = %d, ",thread_count);
    printf ("elapsed time = %g\n",end_time-start_time);
    printf ("sum = %llu\n",sum);
    printf ("N*(N+1)/2 = %llu\n",(N/2)*(N+1));
}

