#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

typedef unsigned long long uint64_t;

int main(int argc, char **argv) {

    /* get N and thread_count from command line */
    if (argc < 3) {
        printf ("Command usage : %s %s %s\n",argv[0],"N","thread_count");
        return 1;
    }
    uint64_t N = atol(argv[1]);
    int thread_count = atoi(argv[2]);
    omp_set_num_threads(thread_count);

    /* start the timer */
    double start_time, end_time;
    start_time = omp_get_wtime();

    /* calculate the sum */
    uint64_t sum = 0;

    // The code now computes the correct value of sum and 
    // the work is distributed approximately evenly between threads. 
    // However, adding more threads still increases the runtime!!
    // To fix the read/write race condition we needed to use a critical region.
    // The issue is that critical regions are expensive because they require
    // synchronization of the threads.  This synchronization also reduces parallelism
    // because will have to wait on other threads to enter the critical region. 
    //
    // To fix this remaining issue, change the code so that each thread only has to 
    // enter the critical region one time inside the parallel region.  

#pragma omp parallel default(none) shared(N,sum,thread_count)
    {
        int thread_num = omp_get_thread_num();
        uint64_t thread_sum = 0;
        for (uint64_t i = 1+thread_num; i <= N;i+=thread_count) {
            thread_sum += i;
        }
#pragma omp critical
        {
            sum += thread_sum;
        }
    }

    /* stop the timer */
    end_time = omp_get_wtime();

    printf ("thread_count = %d, ",thread_count);
    printf ("elapsed time = %g\n",end_time-start_time);
    printf ("sum = %llu\n",sum);
    printf ("N*(N+1)/2 = %llu\n",(N/2)*(N+1));
}

