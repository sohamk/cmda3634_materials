#include <stdio.h>
#include "pri_queue.h"

int main () {

    /* read the number of entries */
    int rows;
    if (scanf("%*c %d",&rows) != 1) {
        printf ("error reading the number of entries!\n");
        return 1;
    }

    /* initialize priority queue */
    pri_queue pq;
    pri_queue_init (&pq,rows);

    /* read entries from file and add them to the priority queue */
    pri_queue_element next;
    for (int i=0;i<rows;i++) {
        if (scanf("%lf",&(next.priority)) != 1) {
            printf ("error : file too small\n");
            return 1;
        }
        next.priority *= -1.0;
        pri_queue_insert (&pq,next);
    }

    /* print the entries from largest to smallest */
    while (pri_queue_size(&pq) > 0) {
        next = pri_queue_peek_top(&pq);
        printf ("%.0lf\n",-1.0*next.priority);
        pri_queue_delete_top(&pq);
    }

    /* free up the priority queue */
    pri_queue_free (&pq);
}
