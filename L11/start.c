#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_NUM_WORDS 20000
#define WORD_SIZE 5
#define SCANF_FORMAT "%6s"

char words[MAX_NUM_WORDS][WORD_SIZE+1];

int main (int argc, char** argv) {

    /* read k from the command line */
#if 0
    if (argc < 2) {
        printf ("error : command usage %s %s\n",argv[0],"k");
        return 1;
    }
    int k = atoi(argv[1]);
#endif

    /* read the valid Wordle words from the file */
    int num_words = 0;
    char word[WORD_SIZE+2];
    while (scanf(SCANF_FORMAT,word) == 1) {
        if (strlen(word) != WORD_SIZE) {
            printf ("error : length of word %d in file is not WORD_SIZE!\n",num_words+1);
            return 1;
        }
        if (num_words >= MAX_NUM_WORDS) {
            printf ("error : too many words in file!\n");
            return 1;
        }
        /* should also check that each word has only lower case letters (exercise) */
        strcpy(words[num_words],word);
        num_words += 1;
    }

    /* count the number of times each letter occurs in each blank */
    int count[WORD_SIZE][26];
    for (int i = 0;i<WORD_SIZE;i++) {
        for (int j = 0;j<26;j++) {
            count[i][j] = 0;
        }
    }
    for (int i=0;i<num_words;i++) {
        for (int j=0;j<WORD_SIZE;j++) {
            count[j][words[i][j]-'a'] += 1;
        }
    }

    /* compute a score for each word and keep track of the max score */
    int max_score = 0;
    int best_starting_word_index;
    for (int i=0;i<num_words;i++) {
        int score = 0;
        for (int j=0;j<WORD_SIZE;j++) {
            score += count[j][words[i][j]-'a'];
        }
        if (score > max_score) {
            max_score = score;
            best_starting_word_index = i;
        }
    }

    /* print max score and best starting word */
    printf ("max score is %d\n",max_score);
    printf ("best starting word is %s\n",words[best_starting_word_index]);

}
