import sys # for command line arguments
from PIL import Image, ImageOps # for image processing
import numpy as np # for matrix processing

# make sure command line arguments are provided
if (len(sys.argv) < 3):
    print ('command usage :',sys.argv[0],'color_image','gray_image')
    exit(1)

# gray = 0.299*red+0.587*green+0.114*blue
