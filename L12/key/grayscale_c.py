import sys # for command line arguments
from PIL import Image, ImageOps # for image processing
import numpy as np # for matrix processing
import ctypes as ct # for calling C from Python

# load C grayscale function
lib = ct.cdll.LoadLibrary("./grayscale.so")

# make sure command line arguments are provided 
if (len(sys.argv) < 3):
    print ('command usage :',sys.argv[0],'color_image','gray_image')
    exit(1)

# open the color image and convert it to a 3d array
color_image = Image.open(sys.argv[1])
A = np.array(color_image)
A_cptr = A.ctypes.data_as(ct.POINTER(ct.c_ubyte))
rows,cols,colors = A.shape
print ('A has',rows,'rows,',cols,'columns, and',colors,'color channels')
print ('datatype of A is',A.dtype)
print ('flags for A are:')
print (A.flags)
#print (A[:,:,0].flags) # not C contiguous

# create a 2d array for the grayscale image
G = np.zeros((rows,cols),dtype='uint8')
G_cptr = G.ctypes.data_as(ct.POINTER(ct.c_ubyte))

# use the C function to convert to grayscale
lib.grayscale(A_cptr,G_cptr,ct.c_int(rows),ct.c_int(cols))

# convert the 2d array of gray intensity values to an image
gray_image = Image.fromarray(G)

# save the grayscale image
gray_image.save(sys.argv[2])
