#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main (int argc, char** argv) {
    /* the type of a string in C is char* */
    char* s1 = "hello!";

    /* printing the string s1 */
    printf ("the string s1 prints to %s\n",s1);

    /* print the length of string s1 */
    printf ("s1 has length %ld\n",strlen(s1));

    /* print the number of command line arguments */
    printf ("number of command line arguments is %d\n",argc);

    /* off by 1? */
    printf ("the first command line argument is %s\n",argv[0]);

    /* we can give strings a new name (we are copying pointers here!) */
    char* name = argv[1];

    /* output a greeting */
    printf ("hello %s!\n",name);

    /* add the two numbers */
    int num1 = atoi (argv[2]);
    int num2 = atoi (argv[3]);
    printf ("The sum of %s and %s is %d\n",argv[2],argv[3],num1+num2);

    /* an array of strings */
    char* msgs[2] = { "Hello World!", "Let's go Hokies!" };
    printf ("%s and %s\n",msgs[0],msgs[1]);

    /* copying strings */
    char msg_copies[2][20];
    if (strlen(msgs[0])+1 < 20) {
        strcpy(msg_copies[0],msgs[0]);
    }
    if (strlen(msgs[1])+1 < 20) {
        strcpy(msg_copies[1],msgs[1]);
    }
    printf ("%s and %s\n",msg_copies[0],msg_copies[1]);    

}
