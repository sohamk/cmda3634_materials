#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_NUM_WORDS 20000
#define WORD_SIZE 5

char words[MAX_NUM_WORDS][WORD_SIZE+1];

typedef struct score_key_s {
    int score;
    int key; 
} score_key_type;

int read_words () {

    /* read the words from the file */
    int num_words = 0;
    char word[BUFSIZ];
    while (scanf("%s",word) == 1) {
        if (num_words >= MAX_NUM_WORDS) {
            printf ("error : too many words in file!\n");
            exit(1);
        }
        if (strlen(word) != WORD_SIZE) {
            printf ("error : length of word %s in file is not WORD_SIZE!\n",word);
            exit(1);
        }
        strcpy(words[num_words],word);
        num_words += 1;
    }
    return num_words;
}

int main (int argc, char** argv) {

    /* read k from the command line */
    if (argc < 2) {
        printf ("error : command usage %s %s\n",argv[0],"k");
        return 1;
    }
    int k = atoi(argv[1]);

    /* read the file of words */
    int num_words = read_words();

    /* count the number of times each letter occurs in each blank */
    int count[WORD_SIZE][26];
    for (int i = 0;i<WORD_SIZE;i++) {
        for (int j = 0;j<26;j++) {
            count [i][j] = 0;
        }
    }
    for (int i=0;i<num_words;i++) {
        for (int j=0;j<WORD_SIZE;j++) {
            count[j][words[i][j]-'a'] += 1;
        }
    }

    /* compute a score key pair for each word */
    score_key_type pairs[num_words];
    for (int i=0;i<num_words;i++) {
        pairs[i].score = 0;
        pairs[i].key = i;
        for (int j=0;j<WORD_SIZE;j++) {
            pairs[i].score += count[j][words[i][j]-'a'];
        }
    }

    /* use a modified insertion sort to sort score key pairs starting from largest score */
    for (int n=0;n<num_words;n++) {
        score_key_type pair = pairs[n];
        int place = 0;  
        while (pair.score < pairs[place].score && place < n) {
            place += 1;
        }
        for (int i=n;i>place;i--) {
            pairs[i] = pairs[i-1];
        }
        pairs[place] = pair;
    }

    /* print top k scores key pairs */
    for (int i=0;i<k;i++) {
        printf ("word %s has score %d\n",words[pairs[i].key],pairs[i].score);
    }

}
