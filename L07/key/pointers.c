#include <stdio.h>

int main () {
  int a = 4;
  int* p1 = &a;
  *p1 = 5;
  printf ("%d\n",a);

  int x[3] =  { 1, 2, 3 };
  int* p2 = x;
  p2[2] = 4;
  printf ("%d %d %d\n",x[0],x[1],x[2]);
}
