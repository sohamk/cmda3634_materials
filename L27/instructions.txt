conda activate cmda3634_master
gcc -O3 -fPIC -shared -o nearest.so nearest.c
gcc -O3 -fPIC -shared -o omp_nearest.so omp_nearest.c -fopenmp
