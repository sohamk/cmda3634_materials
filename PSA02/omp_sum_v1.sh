#!/bin/bash
#SBATCH -A cmda3634_rjh
#SBATCH -p normal_q
#SBATCH -t 00:05:00
#SBATCH --cpus-per-task=4
#SBATCH -o omp_sum_v1.out

# Go to the directory where the job was submitted
cd $SLURM_SUBMIT_DIR

# Load the modules
module load matplotlib

# Build the executable
gcc -o omp_sum_v1 omp_sum_v1.c -fopenmp

# OpenMP settings
export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
export OMP_PROC_BIND=TRUE

# run omp_sum
./omp_sum_v1 200000000 1
./omp_sum_v1 200000000 2
./omp_sum_v1 200000000 4
