#include <stdio.h>
#include <stdlib.h>
#include <float.h>
#include <omp.h>
#include "vec.h"

/* calculate the arg max */
int calc_arg_max (double* data, int rows, int cols, int* centers, int m) {
    int arg_max;
    double cost_sq = 0;
    for (int i=0;i<rows;i++) {
        double min_dist_sq = DBL_MAX;
        for (int j=0;j<m;j++) {
            double dist_sq = vec_dist_sq(data+i*cols,data+centers[j]*cols,cols);
            if (dist_sq < min_dist_sq) {
                min_dist_sq = dist_sq;
            }
        }
        if (min_dist_sq > cost_sq) {
            cost_sq = min_dist_sq;
            arg_max = i;
        }
    }
    return arg_max;
}

/* find the index of the cluster for the given point */
int find_cluster (double* kmeans, double* point, int k, int cols) {
    int cluster;
    double min_dist_sq = DBL_MAX;
    for (int i=0;i<k;i++) {
        double dist_sq = vec_dist_sq(kmeans+i*cols,point,cols);
        if (dist_sq < min_dist_sq) {
            min_dist_sq = dist_sq;
            cluster = i;
        }
    }
    return cluster;
}

/* calculate the next kmeans */
void calc_kmeans_next (double *data, int rows, int cols, double* kmeans, double* kmeans_next, int k) {
    int num_points[k];
    for (int i=0;i<k;i++) {
        num_points[i] = 0;
    }
    vec_zero(kmeans_next,k*cols);
    for (int i=0;i<rows;i++) {
        int cluster = find_cluster(kmeans,data+i*cols,k,cols);
        double* kmean = kmeans_next+cluster*cols;
        vec_add(kmean,data+i*cols,kmean,cols);
        num_points[cluster] += 1;
    }
    for (int i=0;i<k;i++) {
        double* kmean = kmeans_next+i*cols;	
        if (num_points[i] > 0) {
            vec_scalar_mult(kmean,1.0/num_points[i],kmean,cols);
        } else {
            printf ("error : cluster has no points!\n");
            exit(1);
        }
    }
}

/* calculate kmeans using m steps of Lloyd's algorithm */
void calc_kmeans (double *data, int rows, int cols, double* kmeans, int k, int num_iter) {

    /* find k centers using the farthest first algorithm */
    int centers[k];
    centers[0] = 0;
    for (int m=1;m<k;m++) {
        centers[m] = calc_arg_max(data,rows,cols,centers,m);
    }

    /* initialize kmeans using the k centers */
    for (int i=0;i<k;i++) {
        vec_copy(kmeans+i*cols,data+centers[i]*cols,cols);
    }

    /* update kmeans num_iter times */
    double kmeans_next[k*cols];
    for (int i=0;i<num_iter;i++) {
        calc_kmeans_next(data,rows,cols,kmeans,kmeans_next,k);
        vec_copy(kmeans,kmeans_next,k*cols);
    }
}

int main (int argc, char** argv) {

    /* get k, m, and thread_count from command line */
    if (argc < 4) {
        printf ("Command usage : %s %s %s %s\n",argv[0],"k","m","thread_count");
        return 1;
    }
    int k = atoi(argv[1]);
    int m = atoi(argv[2]);
    int thread_count = atoi(argv[3]);
    omp_set_num_threads(thread_count);

    /* read the shape of the data matrix */
    int rows, cols;
    if (scanf("%*c %d %d",&rows, &cols) != 2) {
        printf ("error reading the shape of the data matrix\n");
        return 1;
    }

    /* dynamically allocate memory for the data matrix */
    /* note: this line is roughly equivalent to */
    /* double data[rows*cols] */
    /* but the data memory is located on the heap rather than the stack */
    double* data = (double*)malloc(rows*cols*sizeof(double));

    /* read the data matrix */
    for (int i=0;i<rows;i++) {
        for (int j=0;j<cols;j++) {
            if (scanf("%lf",data+i*cols+j) != 1) {
                printf ("error reading data matrix\n");
                return 1;
            }
        }
    }

    /* start the timer */
    double start_time, end_time;
    start_time = omp_get_wtime();

    /* calculate kmeans using m steps of Lloyd's algorithm */
    double kmeans[k*cols];
    calc_kmeans(data,rows,cols,kmeans,k,m);

    /* stop the timer */
    end_time = omp_get_wtime();

#ifdef STUDY
    printf ("(%d,%.4f),",thread_count,(end_time-start_time));
#else
    /* print out the thread count */
    printf ("# thread_count = %d\n",thread_count);

    /* print out wall time used */
    printf ("# wall time used = %g sec\n",end_time-start_time);

    /* print the results */
    for (int i=0;i<k;i++) {
        for (int j=0;j<cols;j++) {
            printf ("%.5lf ",kmeans[i*cols+j]);
        }
        printf ("\n");
    }
#endif

    /* free the dynamically allocated memory */
    free (data);
}
