#include <stdio.h>
#include <math.h>
#include "float2.h"

#define MAX_POINTS 2000

float float2_dist_sq (float2 u, float2 v) {
    float diff_x = u.x - v.x;
    float diff_y = u.y - v.y;
    return diff_x*diff_x + diff_y*diff_y;
}

int main () {

    /* read the dataset */
    float2 points[MAX_POINTS];
    float2 point;
    int num_points = 0;
    while (scanf("%f %f",&(point.x),&(point.y)) == 2) {
        if (num_points >= MAX_POINTS) {
            printf ("error : too many points!\n");
            return 1;
        }
        points[num_points++] = point;
    }

    /* find the extreme pair */
    float max_dist_sq = 0;
    int extreme1, extreme2;
    for (int i=0;i<num_points-1;i++) {
        for (int j=i+1;j<num_points;j++) {
            float dist_sq = float2_dist_sq(points[i],points[j]);
            if (dist_sq > max_dist_sq) {
                max_dist_sq = dist_sq;
                extreme1 = i;
                extreme2 = j;
            }
        }
    }

    /* output the results */
    printf ("Extreme Distance = %lf\n",sqrt(max_dist_sq));
    printf ("Extreme Pair = %d %d\n",extreme1,extreme2);
}
