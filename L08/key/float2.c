#include <stdio.h>
#include "float2.h"

float2 float2_add (float2 u, float2 v) {
    float2 w;
    w.x = u.x + v.x;
    w.y = u.y + v.y;
    return w;
}

float2 float2_scalar_mult (float2 v, float c) {
    float2 w;
    w.x = v.x*c;
    w.y = v.y*c;
    return w;
}

void float2_print (float2 v, char* name) {
    printf ("%s = (%.4f %.4f)\n",name,v.x,v.y);
}
