#include <stdio.h>
#include <stdlib.h>

typedef struct float2_s {
    float x, y;
} float2;

float2 float2_add (float2 u, float2 v) {
    float2 w;
    w.x = u.x + v.x;
    w.y = u.y + v.y;
    return w;
}

float2 float2_scalar_mult (float2 v, float c) {
    float2 w;
    w.x = v.x*c;
    w.y = v.y*c;
    return w;
}

void float2_print (float2 v, char* name) {
    printf ("%s = (%.4f %.4f)\n",name,v.x,v.y);
}

int main (int argc, char** argv) {

    float2 sum = { 0, 0 };
    float2 point;
    int n = 0;
    while (scanf("%f %f",&(point.x),&(point.y)) == 2) {
        sum = float2_add (sum,point);
        n += 1;
    }
    if (n < 1) {
        printf ("Error : not enough points!\n");
        return 1;
    }
    float2 mean = float2_scalar_mult (sum,1.0/n);
    float2_print (mean,"mean");
}
