#include <stdio.h>
#include <stdlib.h>
#include "kmeans.h"

int main (int argc, char** argv) {

    /* get k and m from command line */
    if (argc < 3) {
        printf ("Command usage : %s %s %s\n",argv[0],"k","m");
        return 1;
    }
    int k = atoi(argv[1]);
    int m = atoi(argv[2]);

    /* read the shape of the data matrix */
    int rows, cols;
    if (scanf("%*c %d %d",&rows, &cols) != 2) {
        printf ("error reading the shape of the data matrix\n");
        return 1;
    }

    /* dynamically allocate memory for the data matrix */
    /* note: this line is roughly equivalent to */
    /* double data[rows*cols] */
    /* but the data memory is located on the heap rather than the stack */
    double* data = (double*)malloc(rows*cols*sizeof(double));

    /* read the data matrix */
    for (int i=0;i<rows;i++) {
        for (int j=0;j<cols;j++) {
            if (scanf("%lf",data+i*cols+j) != 1) {
                printf ("error reading data matrix\n");
                return 1;
            }
        }
    }

    /* calculate kmeans using m steps of Lloyd's algorithm */
    double kmeans[k*cols];
    calc_kmeans(data,rows,cols,kmeans,k,m);

    /* print the results */
    for (int i=0;i<k;i++) {
        for (int j=0;j<cols;j++) {
            printf ("%.5lf ",kmeans[i*cols+j]);
        }
        printf ("\n");
    }

    /* free the dynamically allocated memory */
    free (data);
}
